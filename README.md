# TD2 AI26

## Membres

- Clémence Chomel
- Paco Pompeani

## Compilation et exécution

Pour exécuter notre travail : 
```bash
make new
./new
```

Pour exécuter le code original donné dans l'énoncé :
```bash
make old
./old
```
