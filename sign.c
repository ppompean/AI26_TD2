#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

extern int kill();      // For my linter
extern int sigaction(); // For my linter

#include "sigx.h"

#define pereprintf(string, ...) printf("PERE %d : " string "\n", getpid(), __VA_ARGS__)
#define filsprintf(string, ...) printf("FILS %d : " string "\n", getpid(), __VA_ARGS__)
#define pereprintfmin(string) printf("PERE %d : " string "\n", getpid()) // No support for optional arguments in C
#define filsprintfmin(string) printf("FILS %d : " string "\n", getpid()) // No support for optional arguments in C

int n_sig = 0;
pid_t lespid[3]; // tableau des 3 PIDs utilisés par les 2 fils.

void fonc_pere();
void fonc_fils();
void captpere();
void captfils();

struct sigaction sigact;

int main() {
    int n = fork();
    switch (n) {
    case -1:
        pereprintfmin("erreur de fork");
        break;
    case 0:
        // on met à jour les 2 premiers PIDs chez le fils avant le 2e fork
        lespid[0] = getppid();
        lespid[1] = getpid();
        n = fork();
        switch (n) {
        case -1:
            filsprintfmin("erreur de fork");
            break;
        case 0:
            lespid[2] = getpid();
            fonc_fils(1);
            break;
        default:
            lespid[2] = n;
            fonc_fils(0);
        }
        break;
    default:
        fonc_pere();
    }
    return 0;
}

void fonc_pere() {
    int n;
    sigact.sa_handler = captpere;
    sigaction(SIGINT, &sigact, NULL);
    while (1) {
        n = sleep(10);
        pereprintf("temps restant à sleep = %d", n);
    }
}

void mykill(int victim) {
    filsprintf("victim: %d -> %d", victim, lespid[victim]);
    kill(lespid[victim], SIGINT);
}

void fonc_fils(int no_fils) {
    int i = 0;
    initrec(no_fils);
    sigact.sa_handler = captfils;
    sigaction(SIGINT, &sigact, NULL);
    while (i != -1) {
        i = attendreclic();
        switch (i) {
        case 0:
        case 1:
        case 2:
            mykill(i);
            break;
        case 3:
            filsprintfmin("signaler les 3 processus");
            mykill(0);
            mykill(1);
            mykill(2);
        }
    }
    filsprintfmin("fin du fils apres clic sur FIN");
}

void captfils() {
    n_sig++;
    filsprintf("signal n°%d recu", n_sig);
    // rectvert(2); // Buggée
    if (n_sig == 3) {
        exit(1);
    }
}

void captpere() {
    n_sig++;
    pereprintf("signal n°%d recu", n_sig);
    if (n_sig == 3) {
        exit(2);
    }
}
