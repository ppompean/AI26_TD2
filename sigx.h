/* initrec()   : initialise rectangle rouge
 * i = attendreclic(): boucle attente evenements
 * 	sort avec i = 0 si clic dans rect-fin
 * 	sort avec i = 1 si clic dans grand rect. du bas
 * rectvert(n) : met le rectangle en vert pendant n secondes,
 *               puis le remet a sa couleur precedente.
 * detruitrec(): detruit la fenetre rectangle
 * ecritrec (char *buf, int lng) : crire buf dans la fentre
 * 
 */

int initrec();

int attendreclic();

int rectvert(int n);

void detruitrec();

void ecritrec (char *buf,int lng);

